/*
 * The MIT License
 *
 * Copyright 2014 fmiboy.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package clienxt;

import java.util.*;
import javax.swing.table.*;
// This class manages the account table's data.
class AccountTableModel extends AbstractTableModel implements Observer
{
// These are the names for the table's columns.
private static final String[] columnNames = {"Label", "Account",
"Balance", "Progress (sec)", "Status"};
// These are the classes for each column's values.
private static final Class[] columnClasses = {String.class,
String.class, String.class, String.class,String.class};
public ArrayList<AccountGUI> accountList = new ArrayList<>();
// Add a new account to the table.
public void addAccount(AccountGUI account) {
    // Register to be notified when the account changes.
    account.addObserver(this);
    accountList.add(account);
    // Fire table row insertion notification to table.
    fireTableRowsInserted(getRowCount() - 1, getRowCount() - 1);
}
public void updateAccount(String label, String accn, String balance, int deadline, int row) {
    // Register to be notified when the account chanAccountGUI  
    AccountGUI acco = accountList.get(row);
    acco.deleteObservers();
    
    AccountGUI account = new AccountGUI(acco.getLabel(), acco.getAccountN(), acco.getBalance(), acco.getDeadline());
    
    if (label != null )
        account.setLabel(label);
    else 
        account.setLabel(account.getLabel());
    if (accn != null)
        account.setAccountN(accn);
    else
        account.setAccountN(account.getAccountN());
    if (balance != null)
        account.setBalance(balance);
    else 
        account.setBalance(account.getBalance());
    if (deadline > 0){
        account.setDeadline(deadline);
    }
    else
        account.setDeadline(account.getDeadline());
    
    account.addObserver(this);
    accountList.set(row, account);
    fireTableRowsUpdated(row, row);
}
public int getSize(){
    return accountList.size();
}
// Get a account for the specified AccountGUI
public AccountGUI getAccount(int row) {
    return accountList.get(row);
}
// Remove a download from the list.
public void clearAccount(int row) {
    accountList.remove(row);
    // Fire table row deletion notification to table.
    fireTableRowsDeleted(row, row);
}
// Get table's column count.
public int getColumnCount() {
    return columnNames.length;
}
// Get a column's name.
public String getColumnName(int col) {
    return columnNames[col];
}
// Get a column's class.
public Class getColumnClass(int col) {
    return columnClasses[col];
}
// Get table's row count.
public int getRowCount() {
    return accountList.size();
}
// Get value for a specific row and column combination.
public Object getValueAt(int row, int col) {
    AccountGUI account = accountList.get(row);
    switch (col) {
    case 0: // label
        return account.getLabel();
    case 1: // accountN
        return account.getAccountN();
    case 2: // progress 0-100
        return account.getBalance();
    case 3: // deadline
        //Date date = new Date(account.getDeadline() * 1000);
        //SimpleDateFormat sdf = new SimpleDateFormat("EEEE,MMMM d,yyyy h:mm,a");
        return account.getDeadline();//sdf.format(date);
    case 4: // AccountGUI    
        return AccountGUI.STATUSES[account.getStatus()];
    }
    return "";
}
/* Update is called when a Account notifies its
observers of any changes */
@Override
public void update(Observable o, Object arg) {
    int index = accountList.indexOf(o);
    // Fire table row update notification to table.
    fireTableRowsUpdated(index, index);
}

}
