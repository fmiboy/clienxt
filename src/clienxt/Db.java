/*
 * The MIT License
 *
 * Copyright 2014 fmiboy.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package clienxt;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.h2.jdbcx.JdbcConnectionPool;
/**
 *
 * @author fmiboy
 */
public class Db {
    
    private static volatile JdbcConnectionPool cp;
    private static volatile int maxActiveConnections;
    private static final String DB_DRIVER = "org.h2.Driver";
    private static final String DB_CONNECTION = "jdbc:h2:gui_db/accounts;DB_CLOSE_ON_EXIT=FALSE";
    private static final String DB_USER = "sa";
    private static final String DB_PASSWORD = "sa";
    static void init() {
        long maxCacheSize = 0;
        if (maxCacheSize == 0) {
            maxCacheSize = Runtime.getRuntime().maxMemory() / (1024 * 2);
        }
        String dbUrl = "jdbc:h2:gui_db/accounts;DB_CLOSE_ON_EXIT=FALSE";
        if (! dbUrl.contains("CACHE_SIZE=")) {
            dbUrl += ";CACHE_SIZE=" + maxCacheSize;
        }
        LogGui("GUI database jdbc url set to: " + dbUrl);
        cp = JdbcConnectionPool.create(dbUrl, "sa", "sa");
        cp.setMaxConnections(10);
        cp.setLoginTimeout(30);
        Connection con = null;
        try {
            con = cp.getConnection();
            con.setAutoCommit(false);
        } catch (SQLException ex) {
            LogGui(ex.getMessage());
        }
        int activeConnections = cp.getActiveConnections();
        if (activeConnections > maxActiveConnections) {
            maxActiveConnections = activeConnections;
            LogGui("GUI database connection pool current size: " + activeConnections);
        }
        
        try (Statement stmt = con.createStatement()) {
            LogGui("Initializing GUI database");
            stmt.executeUpdate("CREATE TABLE accounts (ID INT NOT NULL AUTO_INCREMENT, label VARCHAR(100), accountId VARCHAR(100), balance VARCHAR(100), phrase VARCHAR(max), flag VARCHAR(2))");
            con.commit();
            //update(nextUpdate);
        } catch (SQLException e) {
            LogGui("GUI - Accounts table already exists!");
        }
    }

    static void shutdown() {
        if (cp != null) {
            try (Connection con = cp.getConnection();
                Statement stmt = con.createStatement()) {
                stmt.execute("SHUTDOWN COMPACT");
                LogGui("GUI database shutdown completed");
            } catch (SQLException e) {
                LogGui(e.getMessage());
            }
            //cp.dispose();
            cp = null;
        }
    }
    private static String nowtime(){
        Date date = new Date();
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(date);
    }
    private static void LogGui(String message){
        System.out.println("["+nowtime()+"] "+message);
    }
    public static Connection getConnection() throws SQLException {
        Connection con = cp.getConnection();
        con.setAutoCommit(false);
        int activeConnections = cp.getActiveConnections();
        if (activeConnections > maxActiveConnections) {
            maxActiveConnections = activeConnections;
            LogGui("Database connection pool current size: " + activeConnections);
        }
        return con;
    }
    public static Connection getDBConnection() {
        
        Connection dbConnection = null;
        try {
            Class.forName(DB_DRIVER);
        } catch (ClassNotFoundException e) {
            LogGui(e.getMessage());
        }
        try {
            dbConnection = DriverManager.getConnection(DB_CONNECTION, DB_USER,DB_PASSWORD);
            return dbConnection;
        } catch (SQLException e) {
            LogGui(e.getMessage());
 	}
        return dbConnection;
    }
    private Db() {} // never

}
